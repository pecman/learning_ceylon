import ceylon.file {
	parsePath,
	Directory
}
import ceylon.collection {
	ArrayList
}

void easyPrint(ArrayList<String> alist, [String, String] pair) { //将给定变量进行排版输出
	if(alist.empty) {
		print(pair[0]);
	} else {
		print(pair[1]);
		for(item in alist) {
			print("\t``item``");
		}
	}
}

shared void showContents() {
	//数据声明段
	value address = process.readLine() else "";
	value home = parsePath(address);
	value dirs = ArrayList<String>();
	value files = ArrayList<String>();
	//主要过程
	if (is Directory dir = home.resource) {
		for (path in dir.childPaths()) {
			if(is Directory p = path.resource) {
				dirs.add("``p``\\");
				continue;
			}
			files.add("``path``");
		}
		easyPrint(dirs, ["No Directories", "Directory:"]);
		easyPrint(files, ["No Files", "Files:"]);
	}
	else {
		print("The Directory does not exist!");
	}
}